from django.test import TestCase, Client
from django.urls import resolve
from .models import coreDatabase
from .views import homepage, delete, confirm
from .forms import formKabar
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class Story6UnitTest(TestCase):
	def test_story6_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_landing_page_is_using_correct_html(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'homepage/homepage.html')

	def test_landing_page_is_written(self):
		self.assertIsNotNone(homepage)

	def test_function_homepage(self):
		found = resolve('/')
		self.assertEqual(found.func, homepage)

	def test_function_delete(self):
		found = resolve('/1/')
		self.assertEqual(found.func, delete)

	def test_function_confirm(self):
		found = resolve('/confirm/')
		self.assertEqual(found.func, confirm)

	def test_models(self):
		coreDatabase.objects.create(namaUser='ppw',kabarUser='tes')
		countall = coreDatabase.objects.all().count()
		self.assertEqual(countall,1)
	
	def test_invalid_status(self):
		status_form = formKabar({
			'status_name': 301*"X"
			})
		self.assertFalse(status_form.is_valid())

class Story7FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story7FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story7FunctionalTest, self).tearDown()

	def test_input_form_yes(self):  
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		time.sleep(5)
		namaUser = selenium.find_element_by_name('Nama')
		time.sleep(3)
		kabarUser = selenium.find_element_by_name('Kabar')
		time.sleep(3)
		submit = selenium.find_element_by_name('submit')


		namaUser.send_keys("Pepew")
		time.sleep(3)
		kabarUser.send_keys("Baik")
		time.sleep(3)

		submit.send_keys(Keys.RETURN)

		selenium.get('http://127.0.0.1:8000/confirm')
		time.sleep(3)

		submit_confirmation = selenium.find_element_by_name("yes")
		submit_confirmation.send_keys(Keys.RETURN)
		time.sleep(3)

		selenium.get('http://127.0.0.1:8000/')

	def test_input_form_cancel(self):

		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		time.sleep(5)
		namaUser = selenium.find_element_by_name('Nama')
		time.sleep(3)
		kabarUser = selenium.find_element_by_name('Kabar')
		time.sleep(3)
		submit = selenium.find_element_by_name('submit')


		namaUser.send_keys("Pepew")
		time.sleep(3)
		kabarUser.send_keys("Baik")
		time.sleep(3)

		submit.send_keys(Keys.RETURN)

		selenium.get('http://127.0.0.1:8000/confirm')
		time.sleep(3)

		submit_confirmation = selenium.find_element_by_name("no")
		submit_confirmation.send_keys(Keys.RETURN)
		time.sleep(3)

		selenium.get('http://127.0.0.1:8000/')

		

	


