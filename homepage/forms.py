from django import forms

class formKabar(forms.Form):
	Nama = forms.CharField(widget = forms.TextInput(attrs={
		'class' : 'form-control',
		'placeholder' : 'Your Name',
		'type' : 'text',
		'required' : True,
		'maxlength' : 300
    }))

	Kabar = forms.CharField(widget = forms.TextInput(attrs={
		'class' : 'form-control',
		'placeholder' : 'How are you?',
		'type' : 'text',
		'required' : True,
		'maxlength' : 300
    }))