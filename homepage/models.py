from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class coreDatabase(models.Model):
	namaUser = models.CharField(blank = False, max_length=300)
	kabarUser = models.CharField(blank = False, max_length = 300)
	waktu = models.DateTimeField(auto_now_add=True, null = True)
	pengguna = models.ForeignKey(User, on_delete = models.CASCADE, null = True)