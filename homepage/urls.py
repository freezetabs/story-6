from django.urls import path
from . import views


urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('<int:pk>/', views.delete, name='delete'),
    path('confirm/', views.confirm, name='confirm'),
]
