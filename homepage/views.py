from django.shortcuts import render, redirect
from . import forms, models


# Create your views here.
def homepage(request):
    if(request.method == "POST"):
        tmp = forms.formKabar(request.POST)
        if(tmp.is_valid()):
            tmp2 = models.coreDatabase()
            tmp2.namaUser = tmp.cleaned_data["Nama"]
            tmp2.kabarUser = tmp.cleaned_data["Kabar"]
            tmp2.save()
        return redirect("/confirm")
    else:
        tmp = forms.formKabar()
        tmp2 = models.coreDatabase.objects.all()
        tmp_dictio = {
            'formKabar' : tmp,
            'coreDatabase' : tmp2
        }
        return render(request, 'homepage/homepage.html', tmp_dictio)

def delete(request, pk):
    if (request.method == "POST"):
        tmp = forms.formKabar(request.POST)
        if(tmp.is_valid()):
            tmp2 = models.coreDatabase()
            tmp2.namaUser = tmp.cleaned_data["Nama"]
            tmp2.kabarUser = tmp.cleaned_data["Kabar"]
            tmp2.save()
        return redirect("/confirm")
    else:
        models.coreDatabase.objects.filter(pk=pk).delete()
        tmp = forms.formKabar()
        tmp2 = models.coreDatabase.objects.all()
        tmp_dictio = {
            'formKabar' : tmp,
            'coreDatabase' : tmp2
        }
        return render(request, 'homepage/homepage.html', tmp_dictio)

def confirm(request):
    latest = models.coreDatabase.objects.last()
    tmp_dictio = {
        'coreDatabase':latest
    }
    return render(request, 'homepage/confirm.html', tmp_dictio)
        

